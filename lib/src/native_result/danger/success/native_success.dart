// Copyright (C) 2024, the bpriver_native_logger_result project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_native_logger_result/bpriver_native_logger_result.dart';

final class NativeSuccess<
        V extends PointerSignature
        ,E extends Exception
    >
    extends
        NativeDanger<V, E>
{

    @override
    final V wrapped;
    @override
    final Log log;

    NativeSuccess(this.wrapped, Heap heap, [Log? log])
    :
        log = log ?? Log.empty()
    {

        if (wrapped is NativeComplete) {

            // NativeComplete の場合 返す値が存在しないので すべて解放.
            heap._freeAll();

        } else {

            // wrapped 以外の pointer を 解放する.
            heap._freeOther(wrapped);

        }

    }

    @override
    V get asExpected => wrapped;

    @override
    E get asException => Result.panic(BpriverNativeLoggerResultErrorB(), Log(classLocation: NativeSuccess<V, E>, functionLocation: 'asException'));

    @override
    Pointer get pointer => wrapped.pointer;

}
