// Copyright (C) 2024, the bpriver_native_logger_result project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_native_logger_result/bpriver_native_logger_result.dart';

/// {@template NativeDanger}
/// {@endtemplate}
sealed class NativeDanger<
        V extends PointerSignature
        ,E extends Exception
    >
    extends
        NativeResult<V>
{

    const NativeDanger();

    /// {@template NativeDanger.asException}
    /// ## Caution
    /// [asException] is unsafe code.<br>
    /// Throwable [BpriverNativeLoggerResultErrorB] wrapped in [Panic] then [wrapped] is not [E].<br>
    /// This method's main uses is simple notation for test code.<br>
    /// {@endtemplate}
    E get asException;

}
