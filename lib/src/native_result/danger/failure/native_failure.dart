// Copyright (C) 2024, the bpriver_native_logger_result project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_native_logger_result/bpriver_native_logger_result.dart';

final class NativeFailure<
        V extends PointerSignature
        ,E extends Exception
    >
    extends
        NativeDanger<V, E>
{

    @override
    final E wrapped;
    @override
    final Log log;

    NativeFailure(this.wrapped, Heap heap, [Log? log])
    :
        log = log ?? Log.empty()
    {

        // すべての pointer を 解放する.
        heap._freeAll();

    }

    @override
    V get asExpected => Result.panic(BpriverNativeLoggerResultErrorA(), Log(classLocation: NativeFailure<V, E>, functionLocation: 'asExpected'));

    @override
    E get asException => wrapped;

    // Failure の場合 native ではない exception を返すので memory の解放が不要.
    @override
    Pointer<Never> get pointer => nullptr;

}
