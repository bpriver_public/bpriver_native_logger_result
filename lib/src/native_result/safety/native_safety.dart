// Copyright (C) 2024, the bpriver_native_logger_result project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_native_logger_result/bpriver_native_logger_result.dart';

/// {@template NativeSafety}
/// {@endtemplate}
final class NativeSafety<
        V extends PointerSignature
    >
    extends
        NativeResult<V>
{

    @override
    final V wrapped;
    @override
    final Log log;

    NativeSafety(this.wrapped, Heap heap, [Log? log])
    :
        log = log ?? Log.empty()
    {

        // wrapped 以外の pointer を 解放する.
        heap._freeOther(wrapped);

    }
 
    @override
    V get asExpected => wrapped;

    @override
    Pointer get pointer => wrapped.pointer;

}
