// Copyright (C) 2024, the bpriver_native_logger_result project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_native_logger_result/bpriver_native_logger_result.dart';

/// {@template NativeResult}
/// 
/// {@endtemplate}
sealed class NativeResult<
        V extends Object
    >
    with
        AggregationPattern
    implements
        LoggerResultInterface
        ,PointerSignature
{

    @override
    Object get wrapped;
    @override
    Log get log;

    const NativeResult();

    /// {@template NativeResult.asExpected}
    /// ## Caution
    /// [asExpected] is unsafe code.<br>
    /// Throwable [BpriverNativeLoggerResultErrorA] wrapped in [Panic] then [wrapped] is not [V].<br>
    /// This method's main uses is simple notation for test code.<br>
    /// {@endtemplate}
    V get asExpected;

    @override
    Map<String, Object> get properties {
        return {
            'wrapped': wrapped,
            'log': log,
        };
    }

}
