// Copyright (C) 2024, the bpriver_native_logger_result project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_native_logger_result/bpriver_native_logger_result.dart';

// NativeComplete では nullptr(address 0)が指定されるが、特に特別な処理は不要と思われる.
/// {@template Heap}
/// 
/// {@endtemplate}
final class Heap
    with
        AggregationPattern
{

    // result で返す値以外で free を実行する.
    // その後の この heap instance は用済み.
    // 生き残った allocator は 次の heap instance に受け継がれていくため.
    final List<Pointer> _pointerList = [];

    Heap();

    List<Pointer> get pointerList => List.from(_pointerList);

    T allocate<T extends PointerSignature>(T pointerSignature) {
        
        _pointerList.add(pointerSignature.pointer);
        
        return pointerSignature;

    }

    T allocateNative<T extends Pointer>(T pointer) {

        _pointerList.add(pointer);
        
        return pointer;

    }

    /// {@template Heap._freeOther}
    /// [unFreePointer] で渡した pointer 以外に対して free を実行する.<br>
    /// [NativeSafety] and [NativeSuccess] の constructor で実行される.
    /// {@endtemplate}
    void _freeOther(PointerSignature unFreePointer) {

        for (final pointer in _pointerList) {
            
            // address が id の役割になっているはずなので.
            if (unFreePointer.pointer.address == pointer.address) continue;
            
            calloc.free(pointer);

        }

    }

    /// {@template Heap._freeAll}
    /// すべてに対して free を実行する.<br>
    /// [NativeFailure] の constructor で実行される.
    /// {@endtemplate}
    void _freeAll() {

        for (final pointer in _pointerList) {
            
            calloc.free(pointer);

        }

    }

    @override
    Map<String, Object> get properties {
        return {
            '_pointerList': _pointerList,
        };
    }

}
