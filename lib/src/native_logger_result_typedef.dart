// Copyright (C) 2024, the bpriver_native_logger_result project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_native_logger_result/bpriver_native_logger_result.dart';

typedef NativeSafetyComplete = NativeSafety<NativeComplete>;
typedef NativeDangerComplete<E extends Exception> = NativeDanger<NativeComplete, E>;
typedef NativeSuccessComplete<E extends Exception> = NativeSuccess<NativeComplete, E>;
typedef NativeFailureComplete<E extends Exception> = NativeFailure<NativeComplete, E>;

typedef FutureNativeDanger<V extends PointerSignature, E extends Exception> = Future<NativeDanger<NativeComplete, E>>;

typedef FutureNativeSafetyComplete = Future<NativeSafety<NativeComplete>>;
typedef FutureNativeDangerComplete<E extends Exception> = Future<NativeDanger<NativeComplete, E>>;
typedef FutureNativeSuccessComplete<E extends Exception> = Future<NativeSuccess<NativeComplete, E>>;
typedef FutureNativeFailureComplete<E extends Exception> = Future<NativeFailure<NativeComplete, E>>;
