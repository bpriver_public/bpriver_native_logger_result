// Copyright (C) 2024, the bpriver_native_logger_result project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_native_logger_result/bpriver_native_logger_result.dart';

/// {@template BpriverNativeLoggerResultException}
/// {@endtemplate}
sealed class BpriverNativeLoggerResultException
    with
        AggregationPattern
    implements
        Exception
        ,LoggerResultMessageSignature
{

    const BpriverNativeLoggerResultException();

    @override
    List<String> get loggerResultMessage;

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }

}
