// Copyright (C) 2024, the bpriver_native_logger_result project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_native_logger_result/bpriver_native_logger_result.dart';

// NativeComplete を用意しないと Danger などで V extends PointerSignature とできない.
//      既存の Compelte を使いまわそうと思うと V extends Object にしなければならなくなる.

/// {@template NativeComplete}
/// [pointer] は nullptr を返す.<br>
/// nullptr は 0 address を指す.
/// {@endtemplate}
final class NativeComplete
    with
        AggregationPattern
    implements
        LoggerResultMessageSignature
        ,PointerSignature
{

    final List<String> loggerResultMessage;

    const NativeComplete([this.loggerResultMessage = const []]);
    
    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }
    
    @override
    Pointer<Never> get pointer => nullptr;

}
