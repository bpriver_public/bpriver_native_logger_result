// Copyright (C) 2024, the bpriver_native_logger_result project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_native_logger_result/bpriver_native_logger_result.dart';

/// {@template BpriverNativeLoggerResultError}
/// {@endtemplate}
/// * [NativeLoggerResultErrorA]
/// {@macro NativeLoggerResultErrorA}
/// * [NativeLoggerResultErrorB]
/// {@macro NativeLoggerResultErrorB}
sealed class BpriverNativeLoggerResultError
    extends
        Error
    with
        AggregationPattern
    implements
        LoggerResultMessageSignature
{

    @override
    List<String> get loggerResultMessage;

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }

}

/// {@template BpriverNativeLoggerResultErrorA}
/// [NativeResult.wrapped] is not value.
/// [NativeResult.wrapped] is exception.
/// Can't call [NativeResult.asExpected].
/// {@endtemplate}
final class BpriverNativeLoggerResultErrorA
    extends
        BpriverNativeLoggerResultError
{

    static const LOGGER_RESULT_MESSAGE = [
        'NativeResult.wrapped is not value.',
        'NativeResult.wrapped is exception.',
        'Can\'t call NativeResult.asExpected.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverNativeLoggerResultErrorA}
    BpriverNativeLoggerResultErrorA();

}

/// {@template BpriverNativeLoggerResultErrorB}
/// [NativeDanger.wrapped] is not exception.
/// [NativeDanger.wrapped] is value.
/// Can't call [NativeDanger.asException].
/// {@endtemplate}
final class BpriverNativeLoggerResultErrorB
    extends
        BpriverNativeLoggerResultError
{

    static const LOGGER_RESULT_MESSAGE = [
        'NativeDanger.wrapped is not exception.',
        'NativeDanger.wrapped is value.',
        'Can\'t call NativeDanger.asException.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverNativeLoggerResultErrorB}
    BpriverNativeLoggerResultErrorB();

}
