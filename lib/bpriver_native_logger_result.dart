// Copyright (C) 2024, the bpriver_native_logger_result project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

library bpriver_native_logger_result;

import 'dart:ffi';

import 'package:ffi/ffi.dart';

import 'package:bpriver_origin/bpriver_origin.dart';

part 'src/native_logger_result_typedef.dart';

part 'src/pointer_signature.dart';

part 'src/complete/native_complete.dart';

part 'src/heap/heap.dart';

part 'src/native_result/native_result.dart';
part 'src/native_result/safety/native_safety.dart';
part 'src/native_result/danger/failure/native_failure.dart';
part 'src/native_result/danger/success/native_success.dart';
part 'src/native_result/danger/native_danger.dart';

part 'src/bpriver_native_logger_result_error/bpriver_native_logger_result_error.dart';
part 'src/bpriver_native_logger_result_exception/bpriver_native_logger_result_exception.dart';
