## 0.5.2
- Update bpriver origin
    - 0.5.2
- Update bpriver debug
    - 0.5.3

## 0.5.1
- Update bpriver origin
    - 0.5.1
- Update bpriver debug
    - 0.5.2

## 0.4.2
- Update bpriver origin
    - 0.4.2
- Update bpriver debug
    - 0.4.4

## 0.4.1
- Update bpriver origin
    - 0.4.1
- Update bpriver debug
    - 0.4.1

## 0.4.0
- Update bpriver origin
    - 0.4.0
- Update bpriver debug
    - 0.4.0

## 0.3.4
- Update bpriver origin
    - 0.3.4
- Update bpriver debug
    - 0.3.4

## 0.3.3
- Update bpriver origin
    - 0.3.3
- Update bpriver debug
    - 0.3.3

## 0.3.0
- Update dart sdk
    - 3.6.0

## 1.0.0

- Initial version